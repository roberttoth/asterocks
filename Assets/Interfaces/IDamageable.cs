﻿using UnityEngine;
using System.Collections;

public interface IDamageable {
    void GetShot(float damage);
    void CollisionDamage(float damage);
	
}
