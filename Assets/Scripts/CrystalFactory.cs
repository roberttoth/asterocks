﻿using UnityEngine;
using System.Collections;

public class CrystalFactory : MonoBehaviour {

    public static CrystalFactory Instance { private set; get; }
    public GameObject CrystalPrefab;
    public Transform CrystalParent;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    public void CreateCrystal(Vector3 position, Quaternion rotation){
        var crystal = Instantiate(CrystalPrefab, position, rotation) as GameObject;
        crystal.name = "Crystal";
        crystal.transform.SetParent(CrystalParent);

        var direction = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        crystal.GetComponent<Rigidbody2D>().velocity = direction * crystal.transform.up * Random.Range(0.1f, 1f);
        crystal.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-360f, 360f);
    }
}
