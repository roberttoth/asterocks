﻿using UnityEngine;
using System.Collections;

public class StartMenuState : MonoBehaviour {

    public int NumberOfAsteroids;
    public GameObject StartMenu;
    
	// Use this for initialization
	void OnEnable () {
        
        for (int i = 0; i < NumberOfAsteroids; i++)
        {
            AsteroidFactory.Instance.CreateAsteroid();
        }
        StartMenu.SetActive(true);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartGame()
    {
        enabled = false;
    }

    public void OnDisable()
    {
        if(StartMenu != null)
            StartMenu.SetActive(false);
        AsteroidFactory.Instance.ClearAsteroids();
        GameManager.Instance.States.GetComponent<AsteroidHuntState>().enabled = true;
    }




}
