﻿using UnityEngine;
using System.Collections;

public class CrystalController : MonoBehaviour , IDamageable {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.ToLower() == "player")
        {
            GameManager.Instance.AddCrystal();
            Destroy(gameObject);
        }
    }

    public void GetShot(float damage)
    {
        Destroy(gameObject);
    }

    public void CollisionDamage(float damage)
    {
        return;
    }
}
