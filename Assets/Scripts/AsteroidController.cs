﻿using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour, IDamageable {

    public float Health;
    public int PercentChanceForCrystals;
    public int DivisionNumber { get { return _divisionNumber; } }
    
    private float _remainingHealth;
    public int _divisionNumber;

    public void Start()
    {
        _remainingHealth = Health;
    }

    public void Init(int division = 0)
    {
        _divisionNumber = division;
    }

    void Update()
    {
        if (_divisionNumber > 2)
        {
            print("Im too small!" + _divisionNumber);
        }
    }

    public void GetDestroyed()
    {
        var result = Random.Range(0, 100);
        if (result < PercentChanceForCrystals)
        {
            ReleaseCrystals(Random.Range(1, 3));
        }

        AsteroidFactory.Instance.RemoveAsteroid(gameObject);
    }

    private void ReleaseCrystals(int numberOfCrystals)
    {
        for (int i = 0; i < numberOfCrystals; i++)
        {
            CrystalFactory.Instance.CreateCrystal(transform.position, transform.rotation);
        }
    }

    public void GetShot(float damage)
    {
        _remainingHealth -= damage;
        if (_remainingHealth <= 0)
            GetDestroyed();
    }

    public void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag.ToLower() == "asteroid")
            return;

        var damagable = coll.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
        if (damagable == null)
        {
            return;
        }

        var targetMass = coll.rigidbody.mass;
        var thisMass = GetComponent<Rigidbody2D>().mass;
        var relVel = coll.relativeVelocity.magnitude;
        var damage = (thisMass/targetMass)*relVel*Globals.DamageModifier;
        damagable.CollisionDamage(damage);

    }





    public void CollisionDamage(float damage)
    {
        throw new System.NotImplementedException();
    }
}
