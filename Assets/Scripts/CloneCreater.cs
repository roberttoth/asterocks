﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CloneCreater : MonoBehaviour
{

    public GameObject ShipClone;

    private const int VERTICAL_CLONE = 0;
    private const int HORIZONTAL_CLONE = 1;
    private const int SPECIAL_CLONE = 2;

    private GameObject[] clones;
    private float ScreenHalfWidth;
    private float ScreenHalfHeight;
    private float EntityHalfWidth;

    // Use this for initialization
    void Start()
    {
        ScreenHalfWidth = Globals.ScreenHalfWidth;
        ScreenHalfHeight = Globals.ScreenHalfHeight;
        var size = gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size;
        EntityHalfWidth = Mathf.Max(size.x, size.y) / 2; 
        clones = new GameObject[3];
    }
    
    public void Update()
    {
        CreateClones();
        DestroyCloneIfTooFarOut();
        TeleportToTransitionedCloneAndDestroyIt();
    }

    private void CreateClones()
    {
        var entityPosition = transform.position;

        //Create Clone

        if (clones[HORIZONTAL_CLONE] == null)
        {
            if (entityPosition.x - EntityHalfWidth < -ScreenHalfWidth)
            {
                var newPosition = new Vector2(transform.position.x + 2*ScreenHalfWidth, transform.position.y);
                CreateClone(newPosition, HORIZONTAL_CLONE);
            }

            if (entityPosition.x + EntityHalfWidth > ScreenHalfWidth)
            {
                var newPosition = new Vector2(transform.position.x - 2*ScreenHalfWidth, transform.position.y);
                CreateClone(newPosition, HORIZONTAL_CLONE);
            }
        }

        if (clones[VERTICAL_CLONE] == null)
        {
            if (entityPosition.y - EntityHalfWidth < -ScreenHalfHeight)
            {
                var newPosition = new Vector2(transform.position.x, transform.position.y + 2*ScreenHalfHeight);
                CreateClone(newPosition, VERTICAL_CLONE);
            }

            if (entityPosition.y + EntityHalfWidth > ScreenHalfHeight)
            {
                var newPosition = new Vector2(transform.position.x, transform.position.y - 2*ScreenHalfHeight);
                CreateClone(newPosition, VERTICAL_CLONE);
            }
        }

        if (clones[VERTICAL_CLONE] != null && clones[HORIZONTAL_CLONE] != null)
        {
            var x = clones[HORIZONTAL_CLONE].transform.position.x;
            var y = clones[VERTICAL_CLONE].transform.position.y;
            Vector2 newPos = new Vector2(x, y);
            CreateClone(newPos, SPECIAL_CLONE);
        }
    }

    private void CreateClone(Vector2 newPosition, int cloneIndex)
    {
        if (clones[cloneIndex] != null)
            return;

        var rot = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        var vel = new Vector2(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);

        var theClone = Instantiate(ShipClone, newPosition, rot) as GameObject;
        theClone.transform.SetParent(ShipClone.transform.parent);
        
        theClone.GetComponent<Rigidbody2D>().velocity = vel;
        theClone.GetComponent<Rigidbody2D>().angularVelocity = GetComponent<Rigidbody2D>().angularVelocity;

        var theScript = theClone.GetComponent<CloneCreater>();
        if (theScript != null)
            Destroy(theScript);

        clones[cloneIndex] = theClone;
    }

    private void TeleportAndDestroy(GameObject theClone)
    {
        var clonePos = new Vector2(theClone.transform.position.x, theClone.transform.position.y);
        Destroy(theClone);
        transform.position = clonePos;
    }

    private void DestroyCloneIfTooFarOut()
    {
        foreach (var clone in clones.Where(c => c != null))
        {
            var pos = clone.transform.position;
            if (pos.x - EntityHalfWidth >= ScreenHalfWidth || pos.x + EntityHalfWidth <= -ScreenHalfWidth || pos.y - EntityHalfWidth >= ScreenHalfHeight || pos.y + EntityHalfWidth <= -ScreenHalfHeight)
            {
                Destroy(clone);
            }
        }
    }

    private void TeleportToTransitionedCloneAndDestroyIt()
    {
        foreach (var clone in clones.Where(c => c != null))
        {
            var pos = clone.transform.position;
            if (pos.x + EntityHalfWidth < ScreenHalfWidth && pos.x - EntityHalfWidth > -ScreenHalfWidth && pos.y + EntityHalfWidth < ScreenHalfHeight && pos.y - EntityHalfWidth > -ScreenHalfHeight)
            {
                TeleportAndDestroy(clone);
                return;
            }
        }
    }

    public void OnDestroy()
    {
        if (clones != null)
        {
            foreach (var clone in clones)
            {
                Destroy(clone);
            }
        }
    }










}
