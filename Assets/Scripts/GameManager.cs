﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
        
    public GameObject States;
    public Text CrystalCountUI;


    private int _level;

    public int CrystalCount { get { return _crystalCount; } }
    private int _crystalCount;

    public static GameManager Instance { private set; get; }

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start () {
		EnableState<StartMenuState> ();
        _level = 1;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            EnableState<GameOverState>();
            GameObject.Find("ShipExplode").GetComponent<ParticleSystem>().Play();
        }
    }

    public void EnableState<T>()
    {
        var comp = States.GetComponent<T>() as MonoBehaviour;
        comp.enabled = true;
    }
  
    public void AddCrystal()
    {
        _crystalCount++;
        UpdateCyrstalCountUI();
    }


    public int GetLevel()
    {
        return _level;
    }

    public void IncreaseLevel(int amount = 1)
    {
        _level += amount;
    }

    private void UpdateCyrstalCountUI()
    {
        CrystalCountUI.text = string.Format("= {0}",_crystalCount);
    }


}

