﻿using UnityEngine;
using System.Collections;

public class UpgradeDescriptionHandler : MonoBehaviour {

    public GameObject Description;
    private RectTransform _rect;
    
    void Start()
    {
        Description.SetActive(false);
        _rect = GetComponent<RectTransform>();
    }

	

    public void OnMouseOver()
    {
        Description.SetActive(true);
        _rect.SetAsLastSibling();

    }

    public void OnMouseExit()
    {
        Description.SetActive(false);
    }

}
