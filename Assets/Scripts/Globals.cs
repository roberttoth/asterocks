﻿using UnityEngine;
using System.Collections;

public class Globals : MonoBehaviour {

    public static float ScreenHalfWidth;
    public static float ScreenHalfHeight;
    public static float MaxInitialAsteroidSpeed;
    public static float DamageModifier;


	// Use this for initialization
	void Awake () {
        ScreenHalfWidth = Camera.main.orthographicSize * Screen.width / Screen.height;
        ScreenHalfHeight = Camera.main.orthographicSize;
        MaxInitialAsteroidSpeed = 3f;
        DamageModifier = 8.4f;
	}

}
