﻿using UnityEngine;
using System.Collections;

public class ShotFactory : MonoBehaviour {

    public static ShotFactory Instance { private set; get; }
    public GameObject Shot;
    public AudioClip FireSfx;

    private int _shotsOnScreen;
    private MetricsCollector _metrics;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _metrics = MetricsCollector.Instance;
    }


    public GameObject CreateShot(Transform at, int maxShots)
    {
        if (_shotsOnScreen == maxShots)
            return null;

        _shotsOnScreen++;
        SoundManager.Instance.LaserSfx(FireSfx);
        _metrics.ShotFired();
        var shot = Instantiate(Shot, at.position, at.rotation) as GameObject;
        return shot;

            
    }

    public void RemoveShot(GameObject shot)
    {
        _shotsOnScreen--;
        Destroy(shot);
    }
}
