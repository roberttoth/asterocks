﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDController : MonoBehaviour {

    public ShipControll ShipController;

    private Slider _shieldSlider;
    
    // Use this for initialization
	void Start () {
        _shieldSlider = GameObject.Find("ShieldSlider").GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
        _shieldSlider.value = ShipController.ShieldHealth;
	}


}
