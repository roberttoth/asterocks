﻿using UnityEngine;
using System.Collections;

public class ExitPortalController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag != "Player")
            return;
        var hqState = GameManager.Instance.States.GetComponent<HQState>();
        hqState.PlayerHasExited();
    }

}
